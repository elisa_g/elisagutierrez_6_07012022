const passwordValidator = require('password-validator');

//Création du schema 
const passwordSchema = new passwordValidator();

//Propriétés du password
passwordSchema
.is().min(8)                                    // Minimum de 8 caractères
.is().max(25)                                   // Maximum de 25 caractères
.has().uppercase(1)                             // Au moins une lettre en majuscule
.has().lowercase(1)                             // Au moins une lettre en minuscule
.has().digits(1)                                // Au moins 1 chiffre
.has().not().spaces()                           // Pas d'espace

module.exports = passwordSchema;